/* 
 * Copyright (C) 2014 Vasilis Vryniotis <bbriniotis at datumbox.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.datumbox.opensource.examples;

import com.datumbox.opensource.classifiers.NaiveBayes;
import com.datumbox.opensource.dataobjects.NaiveBayesKnowledgeBase;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vasilis Vryniotis <bbriniotis at datumbox.com>
 * @see <a href="http://blog.datumbox.com/developing-a-naive-bayes-text-classifier-in-java/">http://blog.datumbox.com/developing-a-naive-bayes-text-classifier-in-java/</a>
 */
public class NaiveBayesExample {

    /**
     * Reads the all lines from a file and places it a String array. In each 
     * record in the String array we store a training example text.
     * 
     * @param url
     * @return
     * @throws IOException 
     */
    public static String[] readLines(URL url) throws IOException {

        Reader fileReader = new InputStreamReader(url.openStream(), Charset.forName("UTF-8"));
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines.toArray(new String[lines.size()]);
    }
    
    /**
     * Main method
     * 
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    
    public static void main(String[] args) throws IOException {
        //map of dataset files
        File trainingDir = new File(NaiveBayesExample.class.getResource("/datasets_odijs/trainingBarometrsOne").getPath());
        File trainingCategoriesDirs[] = trainingDir.listFiles( new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
                }
            }
        );
                
        //loading examples in memory
        Map<String, String[]> trainingExamples = new HashMap<>();
        for( File trainingCategoryDir : trainingCategoriesDirs ) {
            File trainingFiles[] = trainingCategoryDir.listFiles();
            for( File trainingFile : trainingFiles ) {
                trainingExamples.put( trainingCategoryDir.getName(), readLines(trainingFile.toURI().toURL()));
            }
        }
        
        //train classifier
        NaiveBayes nb = new NaiveBayes();
        nb.setChisquareCriticalValue(0.9);//6.63 //0.01 pvalue
        nb.train(trainingExamples);
        
        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();
        System.out.println("Vārdi: "+knowledgeBase.logLikelihoods.size());
        nb = null;
        trainingExamples = null;
        
        
        //Use classifier
        nb = new NaiveBayes(knowledgeBase);
        String exampleEn = "jauks un pūkains";
        String outputEn = nb.predict(exampleEn);
        System.out.format("The sentense \"%s\" was classified as \"%s\".%n", exampleEn, outputEn);
        
        String exampleFr = "Esmu vienkaarsi sokaa.... Un latviesi veel sim merglim ziedoja naudu lai atveselojas. Nozeelojamakais cilveeks Latvijaa.";
        String outputFr = nb.predict(exampleFr);
        System.out.format("The sentense \"%s\" was classified as \"%s\".%n", exampleFr, outputFr);
        
        String exampleDe = "mīkstais zvērs";
        String outputDe = nb.predict(exampleDe);
        System.out.format("The sentense \"%s\" was classified as \"%s\".%n", exampleDe, outputDe);
        
        File testingDir = new File(NaiveBayesExample.class.getResource("/datasets_odijs/trainingConvOne").getPath());
        File testingCategoriesDirs[] = testingDir.listFiles( new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
                }
            }
        );
        
        Map<String,Map<String,Integer>> params=new HashMap<String, Map<String, Integer>>();
        
        for( File testingCategoryDir : testingCategoriesDirs ) {
            File testingFiles[] = testingCategoryDir.listFiles();
            int tested =0;
            int match=0;
            int notmatch=0;
            for( File testingFile : testingFiles ) {
                String examples[] = readLines(testingFile.toURI().toURL());
                for(int i=0;i<examples.length;i++)
                {
                    String output = nb.predict(examples[i]);
                    ++tested;
                    if( output.equals(testingCategoryDir.getName()) ) {
                        ++match;
                    } else {
                        ++notmatch;
                    }
                }
            }
            
            params.put(testingCategoryDir.getName(), new HashMap<String, Integer>());
            params.get(testingCategoryDir.getName()).put("tested",tested);
            params.get(testingCategoryDir.getName()).put("match",match);
            params.get(testingCategoryDir.getName()).put("notmatch",notmatch);
            System.out.format("The category %s tested:%d match:%d notmatch:%d\n", testingCategoryDir.getName(), tested, match, notmatch);
        }
        System.out.println("Neagresivie:");
        System.out.println("Precision: "+(params.get("neagresivs").get("match")/(double)(params.get("neagresivs").get("match")+params.get("agresivs").get("notmatch"))));
        System.out.println("Recall: "+(params.get("neagresivs").get("match")/(double)(params.get("neagresivs").get("tested"))));
        
        System.out.println("Agresivie:");
        System.out.println("Precision: "+(params.get("agresivs").get("match")/(double)(params.get("agresivs").get("match")+params.get("neagresivs").get("notmatch"))));
        System.out.println("Recall: "+(params.get("agresivs").get("match")/(double)(params.get("agresivs").get("tested"))));
        
    }
    
    /*
    public static void main(String[] args) throws IOException {
        //map of dataset files
        File trainingDir = new File(NaiveBayesExample.class.getResource("/datasets/trainingLemmasOneShort").getPath());
        File trainingCategoriesDirs[] = trainingDir.listFiles( new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
                }
            }
        );
                
        //loading examples in memory
        Map<String, String[]> trainingExamples = new HashMap<>();
        for( File trainingCategoryDir : trainingCategoriesDirs ) {
            File trainingFiles[] = trainingCategoryDir.listFiles();
            for( File trainingFile : trainingFiles ) {
                trainingExamples.put( trainingCategoryDir.getName(), readLines(trainingFile.toURI().toURL()));
            }
        }
        
        //train classifier
        NaiveBayes nb = new NaiveBayes();
        nb.setChisquareCriticalValue(0);//6.63 //0.01 pvalue
        nb.train(trainingExamples);
        
        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();
        
         //map of dataset files
        trainingDir = new File(NaiveBayesExample.class.getResource("/datasets/trainingLemmasOne").getPath());
        trainingCategoriesDirs = trainingDir.listFiles( new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
                }
            }
        );
                
        //loading examples in memory
         trainingExamples = new HashMap<>();
        for( File trainingCategoryDir : trainingCategoriesDirs ) {
            File trainingFiles[] = trainingCategoryDir.listFiles();
            for( File trainingFile : trainingFiles ) {
                trainingExamples.put( trainingCategoryDir.getName(), readLines(trainingFile.toURI().toURL()));
            }
        }
        
        //train classifier
       nb = new NaiveBayes();
        nb.setChisquareCriticalValue(0);//6.63 //0.01 pvalue
        nb.train(trainingExamples);
        
        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase2 = nb.getKnowledgeBase();
        
        for(Map.Entry<String, Map<String, Double>> e1 : knowledgeBase.logLikelihoods.entrySet())
        {
            Map<String, Double> map1=e1.getValue();
            Map<String, Double> map2=knowledgeBase2.logLikelihoods.get(e1.getKey());
            
            if(map1.get("neagresivs")>map1.get("agresivs") && map2.get("neagresivs")<map2.get("agresivs"))
            {
                System.err.println(e1.getKey());
            }
        }
    }
    */
}
