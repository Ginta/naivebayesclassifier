/*
 * Copyright (C) 2014 Ginta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package crossvalidation;

import com.datumbox.opensource.classifiers.NaiveBayes;
import com.datumbox.opensource.dataobjects.NaiveBayesKnowledgeBase;
import com.datumbox.opensource.examples.NaiveBayesExample;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ginta
 */
public class Main {
    
    /**
     * Reads the all lines from a file and places it a String array. In each 
     * record in the String array we store a training example text.
     * 
     * @param url
     * @return
     * @throws IOException 
     */
    public static List<String> readLines(URL url) throws IOException {

        Reader fileReader = new InputStreamReader(url.openStream(), Charset.forName("UTF-8"));
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines;
    }
    
    /**
     * Main method
     * 
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    
    public static void main(String[] args) throws IOException {
        
        Map<String, List<String>> dataSources=new HashMap<>();
        dataSources.put("agresivs",readLines(new File("resources/barometrs/agresivi_10k.txt").toURI().toURL()));
        dataSources.put("neagresivs",readLines(new File("resources/barometrs/neagresivi_10k.txt").toURI().toURL()));
        
        Map<String,Map<String,Integer>> global_params=new HashMap<>();
        for( String category :  dataSources.keySet()) 
        {
            global_params.put(category, new HashMap<String, Integer>());
            global_params.get(category).put("tested",new Integer(0));
            global_params.get(category).put("match",new Integer(0));
            global_params.get(category).put("notmatch",new Integer(0));
        }
        
        for(int i=0;i<10;i++)
        {
            Map<String, String[]> trainingExamples = new HashMap<>();
            Map<String, String[]> testExamples = new HashMap<>();
            
            for(Map.Entry<String, List<String>> entry : dataSources.entrySet())
            {
                List<String> data=entry.getValue();
                int test_from=i*data.size()/10;
                int test_to=(i+1)*data.size()/10;
                
                List<String> test_data=new ArrayList<>(data).subList(test_from,test_to);
                testExamples.put(entry.getKey(),test_data.toArray(new String[test_data.size()]));
                
                List<String> training_data=new ArrayList<>(data);
                training_data.subList(test_from,test_to).clear();
                trainingExamples.put(entry.getKey(),training_data.toArray(new String[training_data.size()]));
                
                
            }

            //train classifier
            NaiveBayes nb = new NaiveBayes();
            nb.setChisquareCriticalValue(0.0);//6.63 //0.01 pvalue
            nb.train(trainingExamples);

            //get trained classifier knowledgeBase
            NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();
            //Use classifier
            
            nb = new NaiveBayes(knowledgeBase);

            Map<String,Map<String,Integer>> params=new HashMap<String, Map<String, Integer>>();
            Integer value;

            for( Map.Entry<String, String[]> entry :  testExamples.entrySet() ) {
                int tested=0;
                int match=0;
                int notmatch=0;
                String examples[] = entry.getValue();
                for (String example : examples) {
                    String output = nb.predict(example);
                    ++tested;
                    if( output.equals(entry.getKey()) ) {
                        ++match;
                    } else {
                        ++notmatch;
                    }
                }

                params.put(entry.getKey(), new HashMap<String, Integer>());
                
                params.get(entry.getKey()).put("tested",tested);
                value=global_params.get(entry.getKey()).get("tested")+tested;
                global_params.get(entry.getKey()).put("tested",value);
                
                params.get(entry.getKey()).put("match",match);
                value=global_params.get(entry.getKey()).get("match")+match;
                global_params.get(entry.getKey()).put("match",value);
                
                params.get(entry.getKey()).put("notmatch",notmatch);
                value=global_params.get(entry.getKey()).get("notmatch")+notmatch;
                global_params.get(entry.getKey()).put("notmatch",value);
                
                System.out.format("The category %s tested:%d match:%d notmatch:%d\n", entry.getKey(), tested, match, notmatch);
            }
            
            System.out.println("Neagresivie:");
            System.out.println("Precision: "+(params.get("neagresivs").get("match")/(double)(params.get("neagresivs").get("match")+params.get("agresivs").get("notmatch"))));
            System.out.println("Recall: "+(params.get("neagresivs").get("match")/(double)(params.get("neagresivs").get("tested"))));

            System.out.println("Agresivie:");
            System.out.println("Precision: "+(params.get("agresivs").get("match")/(double)(params.get("agresivs").get("match")+params.get("neagresivs").get("notmatch"))));
            System.out.println("Recall: "+(params.get("agresivs").get("match")/(double)(params.get("agresivs").get("tested"))));
            System.out.println("---------------------------------------");
        }
        
        System.out.println("===========================================");
        
        for( String category :  dataSources.keySet()) 
        {
            System.out.format("The category %s tested:%d match:%d notmatch:%d\n", category, global_params.get(category).get("tested"), global_params.get(category).get("match"), global_params.get(category).get("notmatch"));
        }
        
        System.out.println("Neagresivie:");
        System.out.println("Precision: "+(global_params.get("neagresivs").get("match")/(double)(global_params.get("neagresivs").get("match")+global_params.get("agresivs").get("notmatch"))));
        System.out.println("Recall: "+(global_params.get("neagresivs").get("match")/(double)(global_params.get("neagresivs").get("tested"))));

        System.out.println("Agresivie:");
        System.out.println("Precision: "+(global_params.get("agresivs").get("match")/(double)(global_params.get("agresivs").get("match")+global_params.get("neagresivs").get("notmatch"))));
        System.out.println("Recall: "+(global_params.get("agresivs").get("match")/(double)(global_params.get("agresivs").get("tested"))));
        
    }
}
